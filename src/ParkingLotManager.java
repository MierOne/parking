import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

public class ParkingLotManager {
      public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            Random random = new Random();
            Car car;

            int uniqueNumber = 999;
            int quantityOfCarsInMove;
            int quantityOfTrucksInMove;
            int command = 0;
            int freePlaces;
            int occupiedPlaces;
            int numOfMoves;

            System.out.println("Сколько мест для легковых автомобилей на парковке ?");
            int quantityOfPlacesForCars = scanner.nextInt();
            System.out.println("Сколь мест для грузовых машин на парковке ?");
            int quantityOfPlacesForTrucks = scanner.nextInt();
            int quantityOfPlaces = quantityOfPlacesForCars + quantityOfPlacesForTrucks;
            Park park = new Park(quantityOfPlaces);

            while (true) {
                  for (int i = 0; i < quantityOfPlaces; i++) {
                        if ((park.getCar(i) != null) && (park.getCar(i).getNumberOfMoves() > 0)) {
                              car = park.getCar(i);
                              car.setNumberOfMoves(car.getNumberOfMoves() - 1);
                              park.setCar(car, i);
                              if(car.type.equals("Truck car") & i>=quantityOfPlacesForTrucks){
                                    i++;
                              }
                        }
                  }
                  for (int i = 0; i < quantityOfPlaces; i++) {
                        if (park.getCar(i) != null && park.getCar(i).getNumberOfMoves() == 0) {
                              park.setCar(null, i);
                        }
                  }

                  quantityOfTrucksInMove = random.nextInt(quantityOfPlaces / 3);
                  quantityOfCarsInMove = random.nextInt(quantityOfPlaces / 3);

                  for (int i = 0; i < quantityOfPlacesForTrucks; i++) {
                        if (park.getCar(i) == null & quantityOfTrucksInMove > 0) {
                              numOfMoves = random.nextInt(10);
                              while (quantityOfTrucksInMove > 1 & numOfMoves == 0) {
                                    quantityOfTrucksInMove--;
                                    uniqueNumber++;
                                    numOfMoves = random.nextInt(10);
                              }
                              uniqueNumber++;
                              car = new Car(uniqueNumber, numOfMoves, "truck car");
                              park.setCar(car, i);
                              quantityOfTrucksInMove--;
                        }
                  }
                  for (int i = quantityOfPlacesForTrucks; i < quantityOfPlaces; i++) {
                        if (park.getCar(i) == null) {
                              if (quantityOfCarsInMove > 0) {
                                    numOfMoves = random.nextInt(10);
                                    while (quantityOfCarsInMove > 1 & numOfMoves == 0) {
                                          quantityOfCarsInMove--;
                                          uniqueNumber++;
                                          numOfMoves = random.nextInt(10);
                                    }
                                    uniqueNumber++;
                                    car = new Car(uniqueNumber, numOfMoves, "passenger car");
                                    park.setCar(car, i);
                                    quantityOfCarsInMove--;
                              } else {
                                    if (quantityOfTrucksInMove > 0 & (i != quantityOfPlaces - 1 && park.getCar(i + 1) == null)) {
                                          numOfMoves = random.nextInt(10);
                                          while (quantityOfTrucksInMove > 1 & numOfMoves == 0) {
                                                quantityOfTrucksInMove--;
                                                uniqueNumber++;
                                                numOfMoves = random.nextInt(10);
                                          }
                                          uniqueNumber++;
                                          car = new Car(uniqueNumber, numOfMoves, "Truck car");
                                          quantityOfTrucksInMove--;
                                          park.setCar(car, i);
                                          park.setCar(car, (i + 1));
                                    }
                              }
                        }
                  }

                  if (quantityOfCarsInMove != 0) {
                        System.out.println("На парковке для легк авто не осталось свободных мест! " + alert(park.minMoves(quantityOfPlacesForTrucks,quantityOfPlaces), quantityOfCarsInMove));
                  }
                  if(quantityOfTrucksInMove !=0){
                        System.out.println("На парковке для груз авто не осталось свободных мест! " + alert(park.minMoves(0,quantityOfPlacesForTrucks),quantityOfTrucksInMove));
                  }

                  while (command != 1) {
                        System.out.println("1 - следующий ход \n" +
                                "2 - Количество занятых мест и свободных мест \n" +
                                "3 - Через сколько освободиться ближайшее место \n" +
                                "4 - Вывести список всех мест на парковке и кем они заняты \n" +
                                "5 - Освободить парковку от всех машин");
                        command = scanner.nextInt();
                        if (command == 2) {
                              freePlaces = numOfFreePlaces(park.getCarsOnPark(), quantityOfPlaces);
                              occupiedPlaces = quantityOfPlaces - freePlaces;
                              System.out.println("Сейчас занято: " + occupiedPlaces + " мест.");
                              System.out.println("Сейчас свободно: " + freePlaces + " мест.");
                        }
                        if (command == 3) {
                              System.out.println("Ближайшее место для груз авто освободится через: " + park.minMoves(0,quantityOfPlacesForTrucks) + " ходов.");
                              System.out.println("Ближайшее место для легк авто освободится через: " + park.minMoves(quantityOfPlacesForTrucks,quantityOfPlaces) + " ходов.");
                        }
                        if (command == 4) {
                              for (int i = 0; i < quantityOfPlaces; i++) {
                                    if (i < quantityOfPlacesForTrucks) {
                                          if (park.getCar(i) != null) {
                                                System.out.println("Место для грузовых авто под номером " + (i + 1) + park.getCar(i).info());
                                          } else {
                                                System.out.println("Место для грузовых авто под номером " + (i + 1) + " свободно.");
                                          }
                                    } else {
                                          if (park.getCar(i) != null) {
                                                System.out.println("Место для легковых авто под номером " + (i + 1) + park.getCar(i).info());
                                          } else {
                                                System.out.println("Место для легковых авто под номером " + (i + 1) + " свободно.");
                                          }
                                    }
                              }
                        }
                        if (command == 5) {
                              for (int i = 0; i < quantityOfPlaces; i++) {
                                    park.setCar(null, i);
                              }
                        }
                  }
                  command = 0;
            }
      }

      public static String alert(int minMoves, int goneCars) {
            return goneCars +
                    " машин уехало. Ближайшее парковочное место освободится только через " + minMoves
                    + " ходов.";
      }

      public static int numOfFreePlaces(ArrayList<Car> cars, int quantityOfPlaces) {
            int numOfFreePlaces = 0;
            for (int i = 0; i < quantityOfPlaces; i++) {
                  if (cars.get(i) == null) {
                        numOfFreePlaces++;
                  }
            }
            return numOfFreePlaces;
      }
}