import java.util.ArrayList;

public class Park {
      int quantityOfPlaces;
      ArrayList<Car> carsOnPark = new ArrayList<>();

      public Park(int quantityOfPlaces){
            this.quantityOfPlaces=quantityOfPlaces;
            for (int i = 0; i < quantityOfPlaces; i++) {
                  carsOnPark.add(i, null);
            }
      }

      public Car getCar(int numberOfCar) { return carsOnPark.get(numberOfCar); }

      public void setCar(Car car, int numberOfCar) {carsOnPark.set(numberOfCar, car);
      }

      public int minMoves(int i1, int i2){
            int minMoves=11;
            for (int i = i1; i < i2; i++) {
                  if(carsOnPark.get(i)==null){
                        return 0;
                  }
                  if(carsOnPark.get(i)!=null && carsOnPark.get(i).getNumberOfMoves()<minMoves){
                        minMoves=carsOnPark.get(i).getNumberOfMoves();
                  }
            }
            return minMoves;
      }

      public ArrayList<Car> getCarsOnPark() {
            return carsOnPark;
      }

      public void setCarsOnPark(ArrayList<Car> carsOnPark) {
            this.carsOnPark = carsOnPark;
      }
}