public class Car {
      int number;
      int numberOfMoves;
      String type;

      public Car(int number, int numberOfMoves,String type){
            this.number=number;
            this.numberOfMoves=numberOfMoves;
            this.type=type;
      }

      public int getNumberOfMoves() {
            return numberOfMoves;
      }

      public void setNumberOfMoves(int numberOfMoves) {
            this.numberOfMoves = numberOfMoves;
      }

      public String info() {
            return " занято " + type + ", с уникальным номером " + number + ", который простоит " + numberOfMoves + " ходов.";
      }
}